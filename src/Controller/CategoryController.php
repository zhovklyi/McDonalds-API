<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CategoryOperations;
use App\Controller\MainController;

class CategoryController extends MainController
{
    const PAGE = 1;
    const PER_PAGE = 20;

    public function __construct(CategoryOperations $service)
    {
        $this->service = $service;
    }

    #[Route('/api/categories', name: 'categories', methods: ['GET'])]
    public function getCategories(Request $request)
    {
        return $this->json(
            $this->service->getCategories([
                'page' => $request->query->get('page') ?? self::PAGE,
                'perPage' => $request->query->get('perPage') ?? self::PER_PAGE
            ])
        );
    }

    #[Route('/api/category', name: 'editCategory', methods: ['POST'])]
    public function editCategory(Request $request)
    {
        $request = $this->transformJsonBody($request);
        $result = $this->service->editCategory([
            'id' => $request->request->get('id'),
            'name' => $request->request->get('name')
        ]);

        return $this->json($result);
    }

    #[Route('/api/category', name: 'createCategory', methods: ['PUT'])]
    public function createCategory(Request $request)
    {
        $request = $this->transformJsonBody($request);
        $newCategory = $this->service->createCategory([
            'name' => $request->request->get('name')
        ]);

        if ($newCategory) {
            return $this->json($newCategory);    
        }

        return $this->json([
            'errors' => true,
            'message' => 'Emty fields found.'
        ]);
    }

    #[Route('/api/category', name: 'deleteCategory', methods: ['DELETE'])]
    public function deleteCategory(Request $request)
    {
        return $this->json(
            $this->service->deleteCategory($request->query->get('id'))
        );
    }
}
