<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PositionOperations;
use App\Controller\MainController;

class PositionsController extends MainController
{
    const PAGE = 1;
    const PER_PAGE = 10;

    public function __construct(PositionOperations $service)
    {
        $this->service = $service;
    }

    #[Route('/api/positions', name: 'getPositions', methods: ['GET'])]
    public function getPositions(Request $request)
    {
        return $this->json(
            $this->service->getPositions(
                $request->query->get('category_id'),
                [
                    'page' => $request->query->get('page') ?? self::PAGE,
                    'perPage' => $request->query->get('perPage') ?? self::PER_PAGE
                ]
            )
        );
    }

    #[Route('/api/position', name: 'createPosition', methods: ['PUT'])]
    public function createPostion(Request $request)
    {
        $request = $this->transformJsonBody($request);
        return $this->json(
            $this->service->createPosition([
                'category_id' => $request->request->get('category_id'),
                'product_id' => $request->request->get('product_id')
            ])
        );
    }

    #[Route('/api/position', name: 'editPosition', methods: ['POST'])]
    public function editPosition(Request $request)
    {
        $request = $this->transformJsonBody($request);
        return $this->json(
            $this->service->editPosition([
                'id' => $request->request->get('id'),
                'category_id' => $request->request->get('category_id') ?? false,
                'product_id' => $request->request->get('product_id') ?? false
            ])
        );
    }

    #[Route('/api/position', name: 'deletePosition', methods: ['DELETE'])]
    public function deletePosition(Request $request)
    {
        return $this->json(
            $this->service->deletePosition($request->get('id'))
        );
    }
}
