<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ProductOperations;
use App\Controller\MainController;

class ProductController extends MainController
{
	const PAGE = 1;
	const PER_PAGE = 10;

	public function __construct(ProductOperations $service)
	{
		$this->service = $service;
	}

	#[Route('/api/products', name: 'getProducts', methods: ['GET'])]
	public function getProducts(Request $request)
	{
		return $this->json(
			$this->service->getProducts([
				'page' => $request->query->get('page') ?? self::PAGE,
				'perPage' => $request->query->get('perPage') ?? self::PER_PAGE
			])
		);
	}

	#[Route('/api/product', name: 'createProduct', methods: ['PUT'])]
    public function createCategory(Request $request)
    {
        $request = $this->transformJsonBody($request);
        $newProduct = $this->service->createProduct([
            'name' => $request->request->get('name')
        ]);

        if ($newProduct) {
            return $this->json($newProduct);    
        }

        return $this->json([
            'errors' => true,
            'message' => 'Emty fields found.'
        ]);
    }

	#[Route('/api/product', name: 'editProduct', methods: ['POST'])]
	public function editProduct(Request $request)
	{
		$request = $this->transformJsonBody($request);
		return $this->json(
			$this->service->editProduct([
				'id' => $request->request->get('id'),
				'name' => $request->request->get('name'),
				'price' => $request->request->get('price')
			])
		);
	}

	#[Route('/api/product', name: 'deleteProduct', methods: ['DELETE'])]
    public function deleteProduct(Request $request)
    {
        return $this->json(
            $this->service->deleteProduct($request->query->get('id'))
        );
    }
}