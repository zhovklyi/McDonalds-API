<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use App\Entity\{
	Position,
	Category,
	Product
};

class PositionOperations
{
	public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
	{
		$this->entityManager = $entityManager;
		$this->validator = $validator;
	}

	public function getPositions($category_id, $pageInfo)
	{
		$positions = $this->entityManager
						  ->getRepository(Position::class);
		if (!$category_id) {
			$positions = $positions->findAll();
		} else {
			$positions = $positions->findForCategory(
				$this->entityManager
					 ->getRepository(Category::class)
					 ->find($category_id)
				);
		}

		$pages = intdiv(count($positions), $pageInfo['perPage']);
		$pages += count($positions) % $pageInfo['perPage'] === 0 ? 0 : 1;

		return [
			'data' => array_slice(
				$positions,
				$pageInfo['page'] * $pageInfo['perPage'] - $pageInfo['perPage'],
				$pageInfo['perPage']
			),
			'total' => count($positions),
			'page' => $pageInfo['page'],
			'perPage' => $pageInfo['perPage'],
			'pages' => $pages,
			'categories' => $this->entityManager
								 ->getRepository(Category::class)
								 ->findAll(),
			'products' => $this->entityManager
							   ->getRepository(Product::class)
							   ->findAll()
		];	}

	public function createPosition($data)
	{
		$newPosition = new Position();

		$newPosition->setCategory(
			$this->entityManager
				 ->getRepository(Category::class)
				 ->find($data['category_id'])
		);

		$newPosition->setProduct(
			$this->entityManager
				 ->getRepository(Product::class)
				 ->find($data['product_id'])
		);

		try {
			$this->entityManager->persist($newPosition);
			$this->entityManager->flush();
		} catch (NotNullConstraintViolationException $e) {
			return [
				'errors' => true,
				'message' => $e->getMessage()
			];
		}

		return $newPosition;
	}

	public function editPosition($data)
	{
		$position = $this->entityManager
						 ->getRepository(Position::class)
						 ->find($data['id']);

		if (!$position) {
			return [
				'errors' => true,
				'message' => 'Position not found'
			];
		}

		if ($data['product_id'] !== false) {
			$position->setProduct(
				$this->entityManager
					 ->getRepository(Product::class)
					 ->find($data['product_id'])
			);
		}

		if ($data['category_id'] !== false) {
			$position->setCategory(
				$this->entityManager
					 ->getRepository(Category::class)
					 ->find($data['category_id'])
			);
		}

		try {
			$this->entityManager->flush();
		} catch (NotNullConstraintViolationException $e) {
			return [
				'errors' => true,
				'message' => $e->getMessage()
			];
		}

		return $position;
	}

	public function deletePosition($id)
	{
		$position = $this->entityManager
						 ->getRepository(Position::class)
						 ->find($id);

		if (!$position) {
			return [
				'errors' => true,
				'message' => 'No position found.'
			];
		}

		$this->entityManager->remove($position);
		$this->entityManager->flush();

		return [
			'success' => true
		];
	}
}