<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Category;

class CategoryOperations
{
	public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
	{
		$this->entityManager = $entityManager;
		$this->validator = $validator;
	}

	public function getCategories($pageInfo): array
	{
		$categories = $this->entityManager
						   ->getRepository(Category::class)
						   ->findAll();

		$pages = intdiv(count($categories), $pageInfo['perPage']);
		$pages += count($categories) % $pageInfo['perPage'] === 0 ? 0 : 1;

		return [
			'data' => array_slice(
				$categories,
				$pageInfo['page'] * $pageInfo['perPage'] - $pageInfo['perPage'],
				$pageInfo['perPage']
			),
			'total' => count($categories),
			'page' => $pageInfo['page'],
			'perPage' => $pageInfo['perPage'],
			'pages' => $pages
		];
	}

	public function editCategory($data)
	{
		$category = $this->entityManager
						 ->getRepository(Category::class)
						 ->find($data['id']);

        if (!$category) {
            return $this->json([
                'errors' => true,
                'message' => 'Category not found.'
            ]);
        }

        $category->setName($data['name']);
        $errors = $this->validator->validate($category);

        if (count($errors) > 0) {
            return $this->json([
                'errors' => true,
                'message' => (string) $errors
            ]);
        }

        $this->entityManager->flush();

        return $category;
	}

	public function createCategory($data)
	{
		$category = new Category();
		$category->setName($data['name']);

		$errors = $this->validator->validate($category);

		if (count($errors) > 0) {
			return false;
		}

		$this->entityManager->persist($category);
		$this->entityManager->flush();

		return $category;
	}

	public function deleteCategory($id)
	{
		$category = $this->entityManager
						 ->getRepository(Category::class)
						 ->find($id);

		if (!$category) {
			return [
				'success' => false,
				'message' => 'No such category.'
			];
		}

		$this->entityManager->remove($category);
		$this->entityManager->flush();

		return [
			'success' => 'true'
		];
	}
}