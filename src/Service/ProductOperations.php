<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Product;

class ProductOperations
{
	public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
	{
		$this->entityManager = $entityManager;
		$this->validator = $validator;
	}

	public function getProducts($pageInfo): array
	{
		$products = $this->entityManager
						   ->getRepository(Product::class)
						   ->findAll();

		$pages = intdiv(count($products), $pageInfo['perPage']);
		$pages += count($products) % $pageInfo['perPage'] === 0 ? 0 : 1;

		return [
			'data' => array_slice(
				$products,
				$pageInfo['page'] * $pageInfo['perPage'] - $pageInfo['perPage'],
				$pageInfo['perPage']
			),
			'total' => count($products),
			'page' => $pageInfo['page'],
			'perPage' => $pageInfo['perPage'],
			'pages' => $pages
		];
	}

	public function createProduct($data)
	{
		$newProduct = new Product();
		$newProduct->setName($data['name']);

		$errors = $this->validator->validate($newProduct);

		if (count($errors) > 0) {
			return [
				'errors' => true,
				'message' => (string) $errors
			];
		}

		$this->entityManager->persist($newProduct);
		$this->entityManager->flush();

		return $newProduct;
	}

	public function editProduct($data)
	{
		$product = $this->entityManager
						->getRepository(Product::class)
						->find($data['id']);

		if (!$product) {
			return [
				'errors' => true,
				'message' => 'No product found.'
			];
		}

		$product->setName($data['name']);
		$product->setPrice($data['price']);

		$errors = $this->validator->validate($product);

		if (count($errors) > 0) {
			return [
				'errors' => true,
				'message' => (string) $errors
			];
		}

		$this->entityManager->flush();
		return $product;
	}

	public function deleteProduct($id)
	{
		$product = $this->entityManager
						 ->getRepository(Product::class)
						 ->find($id);

		if (!$product) {
			return [
				'success' => false,
				'message' => 'No such product.'
			];
		}

		$this->entityManager->remove($product);
		$this->entityManager->flush();

		return [
			'success' => 'true'
		];
	}
}