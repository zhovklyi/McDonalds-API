DELETE FROM `position`;
DELETE FROM `category`;
DELETE FROM `product`;

INSERT INTO `category` (`id`, `name`, `image`) VALUES
(1, "Хіт продажів", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063391"),
(2, "Курка", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063764"),
(3, "МакМеню", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063652"),
(4, "Бургери", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063738"),
(5, "Роли", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063057"),
(6, "Хеппі Міл", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063340"),
(7, "Картопля", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063661"),
(8, "Снеки", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726063764"),
(9, "Салати", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2632435923"),
(10, "Десерти", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/3479132706"),
(11, "Гарячі Напої", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7251_Tea.png"),
(12, "Холодні Напої", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4726064059"),
(13, "Холодні Десерти", "https://res.cloudinary.com/glovoapp/w_150,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4068926152"),
(14, "Соуси", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2210_sauce_cranberry.png");

INSERT INTO `product` (`id`, `price`, `name`, `image`) VALUES
( 1, 11700, "БІГ МАК МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881630" ),
( 2, 15400, "БІГ ТЕЙСТІ МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881634" ),
( 3, 10900, "ДАБЛ ЧІЗБУРГЕР МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881629" ),
( 4, 7500, "ЧІКЕН ПРЕМ'ЄР РОЛ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881613" ),
( 5, 6900, "БІГ МАК", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881605" ),
( 6, 5900, "ДАБЛ ЧІЗБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881603" ),
( 7, 10900, "БІГ ТЕЙСТІ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881611" ),
( 8, 7800, "ЧІКЕН МАКНАГЕТС 9ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881616" ), 
( 9, 10900, "МАКЧІКЕН ПРЕМ'ЄР МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881633" ),
( 10, 11800, "МАКЧІКЕН БЕКОН МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881640" ),
( 11, 8900, "ХЕППІ МІЛ ЧІЗБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881627" ),
( 12, 4800, "КУРЯЧІ СТРІПСИ 3ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_3500_3stripsC.png" ),
( 13, 9200, "КУРЯЧІ СТРІПСИ 6 ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881652" ),
( 14, 17800, "КУРЯЧІ СТРІПСИ 12 ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_3502_12stripsD.png" ),
( 15, 16400, "СТРІПСИ ЧІКЕН БОКС (6стріпсів, 9МакНагетс)", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_3503_strip_nug_boxC.png" ),
( 16, 16400, "МІКС ЧІКЕН БОКС (3стріпси, 4крильця, 6МакНагетс)", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_3504_chiken_mixboxC.png" ),
( 17, 6200, "КУРЯЧІ КРИЛЬЦЯ 4ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2522_wings.png" ),
( 18, 9200, "КУРЯЧІ КРИЛЬЦЯ 6ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2523_wings6.png" ),
( 19, 16400, "КУРЯЧІ КРИЛЬЦЯ 12ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2525_wings122B.png" ),
( 20, 15400, "КРИЛА ЧІКЕН БОКС (6крилець, 9МакНагетс)", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2524_box1.png" ),
( 21, 5900, "МАКЧІКЕН ПРЕМ'ЄР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881610" ),
( 22, 7000, "МАКЧІКЕН БЕКОН", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2053_mcchickencbo.png" ),
( 23, 15400, "ЧІКЕН МАКНАГЕТС 20ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881618" ),
( 24, 5800, "ЧІКЕН МАКНАГЕТС 6ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881615" ),
( 25, 3600, "ЧІКЕН МАКНАГЕТС 4ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881617" ),
( 26, 13900, "РОЯЛ БЕКОН МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881645" ),
( 27, 12600, "РОЯЛ ЧІЗБУРГЕР МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881638" ),
( 28, 10900, "ФІЛЕ-О-ФІШ МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881632" ),
( 29, 10800, "ЧІКЕН МАКНАГЕТС 6ШТ МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881635" ),
( 30, 15400, "ДАБЛ РОЯЛ ЧІЗБУРГЕР МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881639" ),
( 31, 12800, "ЧІКЕН МАКНАГЕТС 9ШТ МЕНЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881636" ),
( 32, 8700, "РОЯЛ БЕКОН", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881650" ),
( 33, 7600, "РОЯЛ ЧІЗБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881619" ),
( 34, 5900, "ФІЛЕ-О-ФІШ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881609" ),
( 35, 10900, "ДАБЛ РОЯЛ ЧІЗБУРГЕР", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2186_royal_double.png" ),
( 36, 4300, "ЧІЗБУРГЕР З БЕКОНОМ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881604" ),
( 37, 3600, "ЧІЗБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881601" ),
( 38, 3100, "ГАМБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881600" ),
( 39, 4600, "КАМАМБЕР ТОСТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_1163_McToastCamamber.png" ),
( 40, 7500, "ФІШ РОЛ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881625" ),
( 41, 8500, "РОЛ З КРЕВЕТКАМИ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881622" ),
( 42, 8900, "ХЕППІ МІЛ ГАМБУРГЕР", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881626" ),
( 43, 8900, "ХЕППІ МІЛ ЧІКЕН МАКНАГЕТС 4ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2304_HappyMeals.png" ),
( 44, 4500, "ІГРАШКА АБО КНИГА", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_8080_ToyBook.png" ),
( 45, 2700, "КАРТОПЛЯ ФРІ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881646" ),
( 46, 4600, "КАРТОПЛЯ ФРІ З СИРНИМ СОУСОМ ТА ЦИБУЛЕЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881647" ),
( 47, 5600, "КАРТОПЛЯ ФРІ З СИРНИМ СОУСОМ ТА БЕКОНОМ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881648" ),
( 48, 4700, "КАРТОПЛЯНІ ДІПИ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881649" ),
( 49, 9500, "КРЕВЕТКИ 5 ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881620" ),
( 50, 6400, "СИР КАМАМБЕР 4ШТ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881614" ),
( 51, 14900, "КРЕВЕТКИ 9ШТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2164_Shrimps_9_pcs.png" ),
( 52, 4900, "МІКС САЛАТ З ОЛИВКОВОЮ ОЛІЄЮ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2632435923" ),
( 53, 10200, "БІГ ЧІКЕН САЛАТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_1067_bigchicken_salad.png" ),
( 54, 2500, "ЯБЛУКА СКИБОЧКАМИ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_1022_Apple.png" ),
( 55, 4200, "ДОНАТ КРЕМ-ШОКОЛАД", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/3479132706" ),
( 56, 2800, "ПЕЧИВО ШОКОЛАДНЕ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4079_Cookies_Choco.png" ),
( 57, 6500, "ЧІЗКЕЙК З ПОЛУНИЦЕЮ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4163_Cheesecake Strawberry.png" ),
( 58, 5900, "ЧІЗКЕЙК", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4670841402" ),
( 59, 2800, "МАКПИРІГ ВИШНЕВИЙ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/3995926063" ),
( 60, 2500, "ЧАЙ ЧОРНИЙ 300МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7251_Tea.png" ),
( 61, 4300, "МОККО 295МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7622_mocco1.png" ),
( 62, 2500, "ЧАЙ З БЕРГАМОТОМ 300МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7252_Tea.png" ),
( 63, 2500, "ЧАЙ ЗЕЛЕНИЙ 300МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7253_Tea.png" ),
( 64, 2500, "ЧАЙ МАЛИНОВИЙ 300МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7172_Tea Raspberry.png" ),
( 65, 1900, "ЕСПРЕСО 40МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7600_espresso.png" ),
( 66, 2300, "ПОДВІЙНЕ ЕСПРЕСО 60МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7609_dbl_espresso.png" ),
( 67, 3600, "ФЛЕТ ВАЙТ 155МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7607_flat_white.png" ),
( 68, 3400, "АМЕРИКАНО КЛАСІК 230МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7604_americano_large.png" ),
( 69, 3900, "КАПУЧИНО КЛАСІК 245МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7605_cappuchino_large.png" ),
( 70, 3900, "ЛАТЕ КЛАСІК 295МЛ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881682" ),
( 71, 3900, "АМЕРИКАНО З МОЛОКОМ КЛАСІК 275МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7611_americano_milkL.png" ),
( 72, 3800, "КАКАО КЛАСІК 310МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7178_Cacao.png" ),
( 73, 2300, "КОКА-КОЛА", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881667" ),
( 74, 2300, "ФАНТА", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881668" ),
( 75, 2300, "СПРАЙТ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7060_Sprite_025.png" ),
( 76, 2100, "ВОДА МОРШИНСЬКА 500МЛ СЛАБОГАЗОВАНА", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7143_Morshinska_w_gas1.png" ),
( 77, 2100, "ВОДА МОРШИНСЬКА 500МЛ НЕГАЗОВАНА", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7144_Morshinska_wo_gas1.png" ),
( 78, 1900, "ВОДА МОРШИНСЬКА 330МЛ НЕГАЗОВАНА", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7142_Morshinska_Sportik_wo_gas_0,33l1.png" ),
( 79, 3200, "АПЕЛЬСИНОВИЙ СІК", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881672" ),
( 80, 1800, "СІК ЯБЛУКО-ЧОРНИЦЯ 200МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7099_Juice_Galicia_Apple-Blueberry_Small_packed1.png" ),
( 81, 1800, "СІК ЯБЛУЧНИЙ 200МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_7097_Juice_Galicia_Apple_Small_packed1.png" ),
( 82, 3800, "МАКШЕЙК ШОКОЛАДНИЙ МАЛИЙ 300МЛ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4068926152" ),
( 83, 3800, "МАКШЕЙК ВАНІЛЬНИЙ МАЛИЙ 300МЛ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4068926153" ),
( 84, 3800, "МАКШЕЙК ПОЛУНИЧНИЙ МАЛИЙ 300МЛ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_5020_ShakeStrawberry_small.png" ),
( 85, 3900, "МАКСАНДІ ШОКОЛАДНЕ У ПЛАСТИКОВОМУ СТАКАНЧИКУ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881656" ),
( 86, 3900, "МАКСАНДІ КАРАМЕЛЬНЕ У ПЛАСТИКОВОМУ СТАКАНЧИКУ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4020_SundaeCaramel.png" ),
( 87, 3900, "МАКСАНДІ ПОЛУНИЧНЕ У ПЛАСТИКОВОМУ СТАКАНЧИКУ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4030_SundaeStrawberry.png" ),
( 88, 4900, "МАКФЛУРІ КІТ КАТ ШОКОЛАДНИЙ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/4780881661" ),
( 89, 4900, "МАКФЛУРІ КІТ КАТ КАРАМЕЛЬНИЙ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4092_McFlurryCaramelA.png" ),
( 90, 4900, "МАКФЛУРІ КІТ КАТ ПОЛУНИЧНИЙ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_4093_McFluri_StrawberryA.png" ),
( 91, 1000, "СОУС ЖУРАВЛИНОВИЙ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2210_sauce_cranberry.png" ),
( 92, 1000, "СОУС КЕТЧУП", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2211_Souce_ketchup.png" ),
( 93, 1000, "СОУС БАРБЕКЮ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2230_Souce_BBQ.png" ),
( 94, 1000, "СОУС ГІРЧИЧНИЙ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2359575712" ),
( 95, 1000, "СОУС КАРІ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2250_Sauce Curry.png" ),
( 96, 1000, "СОУС КИСЛО-СОЛОДКИЙ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2260_Souce_kislo-sladkiy.png" ),
( 97, 1000, "СОУС СОЛОДКИЙ ЧІЛІ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2089810590" ),
( 98, 1000, "СОУС МАЙОНЕЗ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2270_Souce_mayonez.png" ),
( 99, 1000, "СОУС 1000 ОСТРОВІВ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2310296163" ),
( 100, 1000, "СОУС СИРНИЙ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/2310296165" ),
( 101, 1000, "СОУС ЧАСНИКОВИЙ", "https://res.cloudinary.com/glovoapp/w_96,h_96,c_thumb,f_auto,q_auto:best/dpr_auto/Products/1838779385" ),
( 102, 1000, "ОЛИВКОВА ОЛІЯ", "https://d3tqkqn8yl74v5.cloudfront.net/TPO-cso_ua_2286_Souce_olivka.png" );

INSERT INTO `position` (`id`, `category_id`, `product_id`) VALUES
( 1, 1, 1 ),
( 2, 1, 2 ),
( 3, 1, 3 ),
( 4, 1, 4 ),
( 5, 1, 5 ),
( 6, 1, 6 ),
( 7, 1, 7 ),
( 8, 1, 8 ),
( 9, 1, 9 ),
( 10, 1, 10 ),
( 11, 1, 11 ),
( 12, 2, 12 ),
( 13, 2, 13 ),
( 14, 2, 14 ),
( 15, 2, 15 ),
( 16, 2, 16 ),
( 17, 2, 17 ),
( 18, 2, 18 ),
( 19, 2, 19 ),
( 20, 2, 20 ),
( 21, 2, 21 ),
( 22, 2, 9 ),
( 23, 2, 22 ),
( 24, 2, 10 ),
( 25, 2, 23 ),
( 26, 2, 4 ),
( 27, 2, 8 ),
( 28, 2, 24 ),
( 29, 2, 25 ),
( 30, 3, 26 ),
( 31, 3, 1 ),
( 32, 3, 10 ),
( 33, 3, 2 ),
( 34, 3, 27 ),
( 35, 3, 9 ),
( 36, 3, 28 ),
( 37, 3, 29 ),
( 38, 3, 3 ),
( 39, 3, 30 ),
( 40, 3, 31 ),
( 41, 4, 32 ),
( 42, 4, 5 ),
( 43, 4, 22 ),
( 44, 4, 7 ),
( 45, 4, 33 ),
( 46, 4, 21 ),
( 47, 4, 34 ),
( 48, 4, 6 ),
( 49, 4, 35 ),
( 50, 4, 36 ),
( 51, 4, 37 ),
( 52, 4, 38 ),
( 53, 4, 39 ),
( 54, 5, 4 ),
( 55, 5, 40 ),
( 56, 5, 41 ),
( 57, 6, 11 ),
( 58, 6, 42 ),
( 59, 6, 43 ),
( 60, 6, 44 ),
( 61, 7, 45 ),
( 62, 7, 46 ),
( 63, 7, 47 ),
( 64, 7, 48 ),
( 65, 8, 12 ),
( 66, 8, 13 ),
( 67, 8, 14 ),
( 68, 8, 15 ),
( 69, 8, 16 ),
( 70, 8, 17 ),
( 71, 8, 18 ),
( 72, 8, 19 ),
( 73, 8, 20 ),
( 74, 8, 25 ),
( 75, 8, 24 ),
( 76, 8, 8 ),
( 77, 8, 49 ),
( 78, 8, 50 ),
( 79, 8, 23 ),
( 80, 8, 51 ),
( 81, 9, 52 ),
( 82, 9, 53 ),
( 83, 9, 54 ),
( 84, 10, 55 ),
( 85, 10, 56 ),
( 86, 10, 57 ),
( 87, 10, 58 ),
( 88, 10, 59 ),
( 89, 11, 60 ),
( 90, 11, 61 ),
( 91, 11, 62 ),
( 92, 11, 63 ),
( 93, 11, 64 ),
( 94, 11, 65 ),
( 95, 11, 66 ),
( 96, 11, 67 ),
( 97, 11, 68 ),
( 98, 11, 69 ),
( 99, 11, 70 ),
( 100, 11, 71 ),
( 101, 11, 72 ),
( 102, 12, 73 ),
( 103, 12, 74 ),
( 104, 12, 75 ),
( 105, 12, 76 ),
( 106, 12, 77 ),
( 107, 12, 78 ),
( 108, 12, 79 ),
( 109, 12, 80 ),
( 110, 12, 81 ),
( 111, 13, 82 ),
( 112, 13, 83 ),
( 113, 13, 84 ),
( 114, 13, 85 ),
( 115, 13, 86 ),
( 116, 13, 87 ),
( 117, 13, 88 ),
( 118, 13, 89 ),
( 119, 13, 90 ),
( 120, 14, 91 ),
( 121, 14, 92 ),
( 122, 14, 93 ),
( 123, 14, 94 ),
( 124, 14, 95 ),
( 125, 14, 96 ),
( 126, 14, 97 ),
( 127, 14, 98 ),
( 128, 14, 99 ),
( 129, 14, 100 ),
( 130, 14, 101 ),
( 131, 14, 102 );